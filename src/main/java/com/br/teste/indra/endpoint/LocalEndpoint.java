package com.br.teste.indra.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.Local;
import com.br.teste.indra.service.LocalService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/locais")
public class LocalEndpoint {
	
	@Autowired
	private LocalService service;
	
	@GetMapping("{estado}")
	@ApiOperation(
		value = "Busca todos os locais pela sigla do estado",
		notes = "Requisição GET para retornar todos os locais pela sigla do estado",
		response = Local[].class
	)	
	public ResponseEntity<?> getBySigla(@PathVariable String estado) {
		return new ResponseEntity<>(service.findByEstadoSigla(estado), HttpStatus.OK);
	}
}
