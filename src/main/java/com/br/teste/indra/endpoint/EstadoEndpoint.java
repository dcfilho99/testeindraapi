package com.br.teste.indra.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.Estado;
import com.br.teste.indra.service.EstadoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/estados")
public class EstadoEndpoint {
	
	@Autowired
	private EstadoService service;
	
	@GetMapping
	@ApiOperation(
		value = "Busca todos os estados",
		notes = "Requisição GET para retornar todos os estados",
		response = Estado[].class
	)	
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{regiao}")
	@ApiOperation(
		value = "Busca todos os estados por região",
		notes = "Requisição GET para retornar todos os estados por região",
		response = Estado[].class
	)	
	public ResponseEntity<?> getAll(@PathVariable String regiao) {
		return new ResponseEntity<>(service.findByRegiaoSigla(regiao), HttpStatus.OK);
	}
}
