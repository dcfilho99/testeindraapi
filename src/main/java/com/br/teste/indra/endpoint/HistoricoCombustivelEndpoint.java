package com.br.teste.indra.endpoint;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.exception.ResourceNotFoundException;
import com.br.teste.indra.facade.ImportarCsvHistoricoCombustivelFacade;
import com.br.teste.indra.repository.projection.MediaPrecoCombustivel;
import com.br.teste.indra.repository.projection.ValorMedioBandeira;
import com.br.teste.indra.repository.projection.ValorMedioMunicipio;
import com.br.teste.indra.service.HistoricoCombustivelService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/historico-combustiveis")
public class HistoricoCombustivelEndpoint {
	
	@Autowired
	private HistoricoCombustivelService service;
	
	@Autowired
	private ImportarCsvHistoricoCombustivelFacade importerFacade;
	
	@GetMapping(path = "/{id}")
	@ApiOperation(
			value = "Pesquisa histórico pelo ID",
			notes = "Requisição GET passando o ID do histórico a ser retornado",
			response = HistoricoCombustivel.class
		)
	public ResponseEntity<?> getById(@PathVariable Long id) {
		Optional<HistoricoCombustivel> historico = service.findById(id);
		if (!historico.isPresent()) {
			throw new ResourceNotFoundException("Histórico não encontrado [" + id + "]");
		}
		return new ResponseEntity<>(historico.get(), HttpStatus.OK);
	}
	
	@GetMapping(path = "/group-by/revenda")
	@ApiOperation(
			value = "Agrupados pela distribuidora",
			notes = "Requisição GET para retornar pesquisa paginada todos os dados agrupados pela revenda/distribuidora",
			response = Page.class
		)
	public ResponseEntity<?> getAllGroupByRevenda(Pageable pageable) {
		pageable = PageRequest.of(pageable.getPageNumber(), 20, Sort.by(Order.asc("revenda.nome")));
		Page<HistoricoCombustivel> list = service.findAll(pageable);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping(path = "/group-by/data-coleta")
	@ApiOperation(
			value = "Agrupados pela data da colta",
			notes = "Requisição GET para retornar pesquisa paginada de todos os dados agrupados pela data da coleta",
			response = Page.class
		)
	public ResponseEntity<?> getAllGroupByDataColeta(Pageable pageable) {
		pageable = PageRequest.of(pageable.getPageNumber(), 20, Sort.by(Order.desc("dtColeta")));
		Page<HistoricoCombustivel> list = service.findAll(pageable);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping(path = "/pesquisa")
	@ApiOperation(
			value = "Pesquisa o histórico",
			notes = "Requisição GET para retornar pesquisa paginada dos históricos pela sigla",
			response = Page.class
		)
	public ResponseEntity<?> pesquisar(@RequestParam(name = "regiaoSigla", required = false) String regiaoSigla,
										Pageable page) {
		Page<HistoricoCombustivel> historicos = service.getByRegiaoSigla(regiaoSigla, page);
		return new ResponseEntity<>(historicos, HttpStatus.OK);
	}
	
	@GetMapping(path = "/pesquisa/media-preco")
	@ApiOperation(
			value = "Média pesquisa paginada de preço de combustível com base no nome do município",
			notes = "Requisição GET para retornar a média de preço de combustível por município",
			response = Page.class
		)
	public ResponseEntity<?> pesquisarMediaPreco(@RequestParam(name = "municipio", required = false) String municipio,
																Pageable pageable) {
		Page<MediaPrecoCombustivel> medias = service.mediaPrecoPorMunicipio(municipio, pageable); 
		return new ResponseEntity<>(medias, HttpStatus.OK);
	}
	
	@GetMapping(path = "/pesquisa/valores-municipio")
	@ApiOperation(
			value = "Valor médio do valor da compra e do valor da venda por município",
			notes = "Requisição GET para retornar pesquisa paginada do valor médio do valor da compra e do valor da venda por município.",
			response = Page.class
		)
	public ResponseEntity<?> pesquisarValoresMunicipio(@RequestParam(name = "municipio", required = false) String municipio,
																		Pageable pageable) {
		Page<ValorMedioMunicipio> valores = service.valoresMedioPorMunicipio(municipio, pageable); 
		return new ResponseEntity<>(valores, HttpStatus.OK);
	}
	
	@GetMapping(path = "/pesquisa/valores-bandeira")
	@ApiOperation(
			value = "Valor médio do valor da compra e do valor da venda por bandeira",
			notes = "Requisição GET para retornar valor médio do valor da compra e do valor da venda por bandeira.",
			response = Page.class
		)
	public ResponseEntity<?> pesquisarValoresBandeira(@RequestParam(name = "bandeira", required = false) String bandeira,
																		Pageable pageable) {
		Page<ValorMedioBandeira> valores = service.valoresMedioPorBandeira(bandeira, pageable);
		return new ResponseEntity<>(valores, HttpStatus.OK);
	}
	
	@PostMapping(path = "/upload-csv", headers = ("content-type=multipart/*"), consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ApiOperation(
			value = "Upload de CSV para importação",
			notes = "Requisição POST multipart/form-data contendo o arquivo CSV para importação."
		)
	public ResponseEntity<?> upload(@RequestParam(name = "file", required = true) MultipartFile file) throws Exception {
		importerFacade.importar(file);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping
	@ApiOperation(
			value = "Insere no Histórico de combustível",
			notes = "Requisição POST para inserir um novo histórico.",
			response = HistoricoCombustivel.class
		)
	public ResponseEntity<?> save(@RequestBody @Valid HistoricoCombustivel historico) {
		return new ResponseEntity<>(service.save(historico), HttpStatus.OK);
	}
	
	@PutMapping
	@ApiOperation(
			value = "Atualiza um Histórico de combustível",
			notes = "Requisição PUT para atualizar um histórico.",
			response = HistoricoCombustivel.class
		)
	public ResponseEntity<?> update(@RequestBody @Valid HistoricoCombustivel historico) {
		return new ResponseEntity<>(service.save(historico), HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/{id}")
	@ApiOperation(
			value = "Exclui um Histórico de combustível",
			notes = "Requisição DELETE para excluir um histórico."
		)
	public ResponseEntity<?> delete(@PathVariable Long id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
