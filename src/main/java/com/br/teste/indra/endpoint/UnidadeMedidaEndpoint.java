package com.br.teste.indra.endpoint;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.types.UnidadeMedidaType;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/unidades-medida")
public class UnidadeMedidaEndpoint {
	
	@GetMapping
	@ApiOperation(
		value = "Busca todas as regiões",
		notes = "Requisição GET para retornar todas as regiões",
		response = UnidadeMedidaType[].class
	)	
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(UnidadeMedidaType.values(), HttpStatus.OK);
	}
}
