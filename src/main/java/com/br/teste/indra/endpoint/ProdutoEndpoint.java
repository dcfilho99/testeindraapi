package com.br.teste.indra.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.teste.indra.entity.Produto;
import com.br.teste.indra.service.ProdutoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/produtos")
public class ProdutoEndpoint {
	
	@Autowired
	private ProdutoService service;
	
	@GetMapping
	@ApiOperation(
		value = "Busca todos os produtos",
		notes = "Requisição GET para retornar todos os produtos",
		response = Produto[].class
	)	
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
	}
}
