package com.br.teste.indra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.teste.indra.entity.Revenda;

@Repository
public interface RevendaRepository extends JpaRepository<Revenda, Long> {

	public Optional<Revenda> findByCnpj(String cnpj);
	public boolean existsByNomeIgnoreCase(String nome);
	public boolean existsByCnpjIgnoreCase(String cnpj);
}
