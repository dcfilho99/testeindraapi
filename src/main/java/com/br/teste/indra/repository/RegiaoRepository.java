package com.br.teste.indra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.teste.indra.entity.Regiao;

@Repository
public interface RegiaoRepository extends JpaRepository<Regiao, Long> {

	Optional<Regiao> findBySigla(String sigla);
}
