package com.br.teste.indra.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.repository.projection.MediaPrecoCombustivel;
import com.br.teste.indra.repository.projection.ValorMedioBandeira;
import com.br.teste.indra.repository.projection.ValorMedioMunicipio;

@Repository
public interface HistoricoCombustivelRepository extends JpaRepository<HistoricoCombustivel, Long> {
	
	public Page<HistoricoCombustivel> findByLocalEstadoRegiaoSiglaContainingIgnoreCase(String regiaoSigla, Pageable pageable);
	public List<HistoricoCombustivel> findByLocalMunicipioContainingIgnoreCase(String municipio);
	public List<HistoricoCombustivel> findByRevendaBandeiraContainingIgnoreCase(String bandeira);
	
	@Query(value = " select " + 
				   "	local.municipio as municipio, " + 
				   " 	produto.nome as produto, " + 
				   "	avg(hist.vlrVenda) as mediaPreco " + 
				   " from HistoricoCombustivel hist " +
				   " inner join hist.local local " +  
				   " inner join hist.produto produto " + 
				   " where " + 
				   " 	(upper(local.municipio) like concat('%', upper(:municipio), '%')) " +  
				   " group by local.municipio, produto.nome " + 
				   " order by local.municipio asc ")
	public Page<MediaPrecoCombustivel> getMediaValorCompraPorMunicipio(@Param("municipio") String municipio, Pageable pageable);
	
	@Query(value = " select " + 
				   "	local.municipio as municipio, " + 
				   " 	produto.nome as produto, " + 
				   "	avg(hist.vlrCompra) as vlrCompra, " +
				   "	avg(hist.vlrVenda) as vlrVenda " + 
				   " from HistoricoCombustivel hist " +
				   " inner join hist.local local " + 
				   " inner join local.estado estado " + 
				   " inner join hist.produto produto " + 
				   " where " + 
				   " 	(upper(local.municipio) like concat('%', upper(:municipio), '%')) " + 
				   " group by local.municipio, produto.nome " + 
				   " order by local.municipio asc ")
	public Page<ValorMedioMunicipio> getValorMedioCompraVendaPorMunicipio(@Param("municipio") String municipio, Pageable pageable);
	
	@Query(value = " select " + 
				   "	revenda.bandeira as bandeira, " + 
				   " 	produto.nome as produto, " + 
				   "	avg(hist.vlrCompra) as vlrCompra, " +
				   "	avg(hist.vlrVenda) as vlrVenda " + 
				   " from HistoricoCombustivel hist " +
				   " inner join hist.revenda revenda " + 
				   " inner join hist.produto produto " + 
				   " where " + 
				   " 	(upper(revenda.bandeira) like concat('%', upper(:bandeira), '%')) " + 
				   " group by revenda.bandeira, produto.nome " + 
				   " order by revenda.bandeira asc ")
	public Page<ValorMedioBandeira> getValorMedioCompraVendaPorBandeira(@Param("bandeira") String bandeira, Pageable pageable);
	
	public Page<HistoricoCombustivel> findAllGroupByRevendaNomeOrderByRevendaNome(Pageable pageable);
}
