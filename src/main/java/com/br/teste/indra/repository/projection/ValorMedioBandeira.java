package com.br.teste.indra.repository.projection;

import java.math.BigDecimal;

public interface ValorMedioBandeira {

	String getBandeira();
	String getProduto();
	BigDecimal getVlrVenda();
	BigDecimal getVlrCompra();
}
