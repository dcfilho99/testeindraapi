package com.br.teste.indra.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "produto")
public class Produto implements Serializable {

	private static final long serialVersionUID = -5577865253809657620L;
	
	@Id
	@GeneratedValue(generator = "produto_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "produto_id_seq", sequenceName = "produto_id_seq")
	private Long id;
	
	@NotEmpty
	@Column(name = "nome", unique = true)
	private String nome;
}
