package com.br.teste.indra.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.br.teste.indra.entity.types.UnidadeMedidaType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor 
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "historico_combustivel")
public class HistoricoCombustivel implements Serializable {

	private static final long serialVersionUID = 46541435092156732L;
	
	@Id
	@GeneratedValue(generator = "hist_comb_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "hist_comb_id_seq", sequenceName = "hist_comb_id_seq")
	private Long id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "local_id")
	private Local local;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "revenda_id")
	private Revenda revenda;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "produto_id")
	private Produto produto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dt_coleta")
	private Date dtColeta;
	
	@Column(name = "vlr_compra", precision=10, scale=3)
	@Type(type = "big_decimal")
	private BigDecimal vlrCompra;
	
	@Column(name = "vlr_venda", precision=10, scale=3)
	@Type(type = "big_decimal")
	private BigDecimal vlrVenda;
	
	@Column(name = "unidade_medida")
	@Enumerated(EnumType.STRING)
	private UnidadeMedidaType unidadeMedida;
}
