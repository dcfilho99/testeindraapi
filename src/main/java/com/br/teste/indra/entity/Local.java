package com.br.teste.indra.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode
@Entity
@Table(name = "local")
public class Local implements Serializable {

	private static final long serialVersionUID = -266983301174581153L;

	@Id
	@GeneratedValue(generator = "local_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "local_id_seq", sequenceName = "local_id_seq")
	private Long id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "estado_id")
	private Estado estado;
	
	@NotEmpty
	private String municipio;

}
