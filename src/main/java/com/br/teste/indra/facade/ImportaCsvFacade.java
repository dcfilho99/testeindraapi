package com.br.teste.indra.facade;

import org.springframework.web.multipart.MultipartFile;

public abstract class ImportaCsvFacade {

	public abstract void importar(MultipartFile file);
}
