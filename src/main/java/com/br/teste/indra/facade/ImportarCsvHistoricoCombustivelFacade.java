package com.br.teste.indra.facade;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.br.teste.indra.adapter.MultipartfileAdapter;
import com.br.teste.indra.entity.Estado;
import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Local;
import com.br.teste.indra.entity.Produto;
import com.br.teste.indra.entity.Regiao;
import com.br.teste.indra.entity.Revenda;
import com.br.teste.indra.service.EstadoService;
import com.br.teste.indra.service.HistoricoCombustivelService;
import com.br.teste.indra.service.LocalService;
import com.br.teste.indra.service.ProdutoService;
import com.br.teste.indra.service.RegiaoService;
import com.br.teste.indra.service.RevendaService;
import com.br.teste.indra.strategy.ContextCsvConversao;
import com.br.teste.indra.strategy.CsvConversaoHistoricoStrategy;

@Component
public final class ImportarCsvHistoricoCombustivelFacade extends ImportaCsvFacade {
	
	@Autowired
	private HistoricoCombustivelService service;
	
	@Autowired
	private RevendaService revendaService;
	
	@Autowired
	private RegiaoService regiaoService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private LocalService localService;
	
	@Autowired
	private EstadoService estadoService;
	
	@SuppressWarnings("unchecked")
	@Override
	public void importar(MultipartFile file) {
		try {
			MultipartfileAdapter adapter = new MultipartfileAdapter(file);
			File arquivo = adapter.adaptar();
			
			ContextCsvConversao context = new ContextCsvConversao(new CsvConversaoHistoricoStrategy());
			context.converter(arquivo);
			List<HistoricoCombustivel> historicos = (List<HistoricoCombustivel>) context.getResultList();
			
            iniciarRegioes(historicos);
            iniciarEstados(historicos);
            iniciarLocais(historicos);
            iniciarProdutos(historicos);
            iniciarRevendas(historicos);			
            iniciarHistoricos(historicos);
            
            arquivo.deleteOnExit();
        } catch (Exception ex1) {
            System.out.println("Erro lendo arquivo. " + ex1.getLocalizedMessage());
        }
	}
	
	private void iniciarEstados(List<HistoricoCombustivel> historicos) {
		List<Estado> estados = new ArrayList<>();
		
		Set<Estado> produtosAsStr = (Set<Estado>) historicos.stream()
										.map(item -> item.getLocal().getEstado())
										.collect(Collectors.toSet());
		
		for (Estado prd : produtosAsStr) {
			Optional<Estado> opt = estadoService.findBySigla(prd.getSigla());
			if (opt.isPresent()) {
				prd = opt.get();
			}	
			estados.add(prd);
		}
		estados = estadoService.saveAll(estados);
		
		flushEstados(historicos, estados);
	}

	private void flushEstados(List<HistoricoCombustivel> historicos, List<Estado> estados) {
		for (HistoricoCombustivel hist : historicos) {
			String sigla = hist.getLocal().getEstado().getSigla();
			
			Estado est = estados.stream()
							.filter(
								item -> item.getSigla().toUpperCase().equals(sigla.toUpperCase())
							).findFirst().get();
			
			hist.getLocal().setEstado(est);
		}
	}
	
	private void iniciarLocais(List<HistoricoCombustivel> historicos) {
		List<Local> locais = new ArrayList<Local>();
		
		Set<Local> locaisAsStr = (Set<Local>) historicos.stream()
										.map(item -> item.getLocal())
										.collect(Collectors.toSet());
		
		for (Local l : locaisAsStr) {
			Optional<Local> opt = localService.findByMunicipioAndEstadoSigla(l.getMunicipio(), l.getEstado().getSigla());
			if (opt.isPresent()) {
				l = opt.get();
			}
			locais.add(l);
		}
		locais = localService.saveAll(locais);		
		
		flushLocais(historicos, locais);
	}

	private void flushLocais(List<HistoricoCombustivel> historicos, List<Local> locais) {
		for (HistoricoCombustivel hist : historicos) {
			String municipio = hist.getLocal().getMunicipio();
			
			Local local = locais.stream()
							.filter(
								item -> item.getMunicipio().toUpperCase().equals(municipio.toUpperCase())
							).findFirst().get();
			
			hist.setLocal(local);
		}
	}
	
	private void iniciarProdutos(List<HistoricoCombustivel> historicos) {
		List<Produto> produtos = new ArrayList<>();
		
		Set<Produto> produtosAsStr = (Set<Produto>) historicos.stream()
										.map(item -> item.getProduto())
										.collect(Collectors.toSet());
		
		for (Produto prd : produtosAsStr) {
			Optional<Produto> opt = produtoService.findByNome(prd.getNome());
			if (opt.isPresent()) {
				prd = opt.get();
			}
			produtos.add(prd);
		}
		produtos = produtoService.saveAll(produtos);
		
		flushProdutos(historicos, produtos);
	}

	private void flushProdutos(List<HistoricoCombustivel> historicos, List<Produto> produtos) {
		for (HistoricoCombustivel hist : historicos) {
			String produto = hist.getProduto().getNome();
			
			Produto prod = produtos.stream()
							.filter(
								item -> item.getNome().toUpperCase().equals(produto.toUpperCase())
							).findFirst().get();
			
			hist.setProduto(prod);
		}
	}
	
	private void iniciarRegioes(List<HistoricoCombustivel> historicos) {
		List<Regiao> regioes = new ArrayList<>();
		
		Set<String> produtosAsStr = (Set<String>) historicos.stream()
										.map(item -> item.getLocal().getEstado().getRegiao().getSigla())
										.collect(Collectors.toSet());
		
		for (String prd : produtosAsStr) {
			Optional<Regiao> opt = regiaoService.findBySigla(prd);
			if (!opt.isPresent()) {
				regioes.add(new Regiao(null, prd));
			} else {
				regioes.add(opt.get());
			}
			
		}
		regioes = regiaoService.saveAll(regioes);
		
		flushRegioes(historicos, regioes);
	}

	private void flushRegioes(List<HistoricoCombustivel> historicos, List<Regiao> regioes) {
		for (HistoricoCombustivel hist : historicos) {
			String sigla = hist.getLocal().getEstado().getRegiao().getSigla();
			
			Regiao reg = regioes.stream()
							.filter(
								item -> item.getSigla().toUpperCase().equals(sigla.toUpperCase())
							).findFirst().get();
			
			hist.getLocal().getEstado().setRegiao(reg);
		}
	}

	private void iniciarRevendas(List<HistoricoCombustivel> historicos) {
		List<HistoricoCombustivel> novoRetorno = new ArrayList<>();
		
		Map<Revenda, List<HistoricoCombustivel>> mapHist = historicos.stream()
																.collect(Collectors.groupingBy(HistoricoCombustivel::getRevenda));
		
		flushRevendas(novoRetorno, mapHist);		
		historicos = novoRetorno;
	}

	private void flushRevendas(List<HistoricoCombustivel> novoRetorno,
			Map<Revenda, List<HistoricoCombustivel>> mapHist) {
		for (Revenda revenda : mapHist.keySet()) {
			List<HistoricoCombustivel> historicos = mapHist.get(revenda);
			
			Optional<Revenda> opt = revendaService.findByCnpj(revenda.getCnpj());
			if (opt.isPresent()) {
				revenda = opt.get();
			} else {
				revenda = revendaService.save(revenda);
			}			
			final Revenda rev = revenda;
			historicos.stream().forEach(item -> item.setRevenda(rev));
			
			novoRetorno.addAll(historicos);
		}
	}
	
	public void iniciarHistoricos(List<HistoricoCombustivel> historicos) {
		service.saveAll(historicos);
        historicos.clear();
	}
}
