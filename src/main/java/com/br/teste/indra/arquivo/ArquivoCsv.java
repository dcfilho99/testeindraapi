package com.br.teste.indra.arquivo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
public class ArquivoCsv implements Arquivo {

	private List<String> linhas;
	private String separator;
}
