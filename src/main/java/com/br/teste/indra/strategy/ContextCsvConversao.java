package com.br.teste.indra.strategy;

import java.io.File;
import java.util.List;

public class ContextCsvConversao {

	private CsvConversaoStrategy strategy;
	
	public ContextCsvConversao(CsvConversaoStrategy strategy) {
		this.strategy = strategy;
	}
	
	public void converter(File file) throws Exception {
		strategy.converter(file);
	}
	
	public List<?> getResultList() {
		return strategy.getResult();
	}
}
