package com.br.teste.indra.strategy;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.br.teste.indra.arquivo.ArquivoCsv;
import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.factory.ArquivoCsvFactory;
import com.br.teste.indra.factory.HistoricoCombustivelFactory;

public class CsvConversaoHistoricoStrategy implements CsvConversaoStrategy {
	
	private final Integer HIST_COMBUSTIVEL_CSV_NR_COLUNAS = 11;
	
	private List<HistoricoCombustivel> historicos = new ArrayList<>();
	
	@Override
	public void converter(File file) {
		try {
			ArquivoCsv csv = ArquivoCsvFactory.fromFileSemCabecalho(file, "\t");
			for (String linha : csv.getLinhas()) {
            	List<String> colunas = Arrays.asList(linha.split(csv.getSeparator()));
            	if (isArquivoCsvHistoricoCombustivelValido(colunas)) {
            		HistoricoCombustivel historico = HistoricoCombustivelFactory.newInstanceFromCsv(colunas);
            		historicos.add(historico);
            	}
	        }            
        } catch (Exception ex1) {
            System.out.println("Erro lendo arquivo. " + ex1.getLocalizedMessage());
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HistoricoCombustivel> getResult() {
		return historicos;
	}
	
	private boolean isArquivoCsvHistoricoCombustivelValido(List<String> colunas) {
		return colunas.size() == HIST_COMBUSTIVEL_CSV_NR_COLUNAS;
	}
}
