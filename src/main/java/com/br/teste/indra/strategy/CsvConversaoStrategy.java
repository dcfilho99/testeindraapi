package com.br.teste.indra.strategy;

import java.io.File;

public interface CsvConversaoStrategy {

	public void converter(File file) throws Exception;
	public <T> T getResult();
}
