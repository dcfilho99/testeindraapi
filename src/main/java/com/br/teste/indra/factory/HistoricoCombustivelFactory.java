package com.br.teste.indra.factory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.br.teste.indra.entity.Estado;
import com.br.teste.indra.entity.HistoricoCombustivel;
import com.br.teste.indra.entity.Local;
import com.br.teste.indra.entity.Produto;
import com.br.teste.indra.entity.Regiao;
import com.br.teste.indra.entity.Revenda;
import com.br.teste.indra.entity.types.UnidadeMedidaType;

public abstract class HistoricoCombustivelFactory {
	
	public static HistoricoCombustivel newInstance() {
		return new HistoricoCombustivel();
	}
	
	public static HistoricoCombustivel newInstanceFromCsv(List<String> colunas) throws ParseException {
		HistoricoCombustivel historico = new HistoricoCombustivel();
		
		Regiao regiao = new Regiao(null, colunas.get(0).trim().toUpperCase());
		
		Estado estado = new Estado(null, regiao, colunas.get(1).trim().toUpperCase());
		
		Local local = new Local(null, estado, colunas.get(2).trim().toUpperCase());
		historico.setLocal(local);
		
		Revenda revenda = new Revenda(null, colunas.get(3).replaceAll("^,", "").toUpperCase(), 
											colunas.get(4).trim(), 
											colunas.get(10).trim().toUpperCase());
		historico.setRevenda(revenda);
		
		Produto produto = new Produto(null, colunas.get(5).trim().toUpperCase());
		historico.setProduto(produto);
    	
    	Date dtStr = new SimpleDateFormat("dd/MM/yyyy").parse(colunas.get(6));
    	historico.setDtColeta(dtStr);
    	
    	if (!"".equals(colunas.get(7))) {
    		String vlrVStr = colunas.get(7).trim().replace(",", ".");
        	historico.setVlrVenda(new BigDecimal(vlrVStr));     	     
    	} else {
    		historico.setVlrVenda(new BigDecimal(0.000));
    	}   
    	
    	if (!"".equals(colunas.get(8))) {
    		String vlrCStr = colunas.get(8).replace(",", ".");
        	historico.setVlrCompra(new BigDecimal(vlrCStr));     	     
    	} else {
    		historico.setVlrCompra(new BigDecimal(0.000));
    	}  	
    	UnidadeMedidaType unidadeMedida = UnidadeMedidaType.fromDescription(colunas.get(9).replace(" ", ""));
    	historico.setUnidadeMedida(unidadeMedida);
    	
    	return historico;
	}
}
