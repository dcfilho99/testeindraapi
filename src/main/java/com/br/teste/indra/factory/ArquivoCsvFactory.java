package com.br.teste.indra.factory;

import java.io.File;
import java.util.List;

import com.br.teste.indra.arquivo.ArquivoCsv;
import com.br.teste.indra.util.LeitorArquivoCsv;

public abstract class ArquivoCsvFactory {

	public static ArquivoCsv fromFileSemCabecalho(File file, String separator) throws Exception {
		LeitorArquivoCsv leitor = new LeitorArquivoCsv(file);
		List<String> linhas = leitor.readLinesSemCabecalho(file);		
		
		ArquivoCsv csv = new ArquivoCsv(linhas, separator);
		return csv;
	}
}
