package com.br.teste.indra.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class LeitorArquivoCsv extends LeitorArquivo {
	
	private File file;
	
	public LeitorArquivoCsv(File file) throws Exception {
		this.file = file;
		this.isCsv();		
	}
	
	public List<String> readLines() throws UnsupportedEncodingException, IOException {
		BufferedReader ler = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-16"));
		
        String linha;
        List<String> linhas = new ArrayList<String>();
        while (ler.ready()) {
        		linha = ler.readLine();
        		linhas.add(linha);
        }
        ler.close();
        return linhas;
	}
	
	public List<String> readLinesSemCabecalho(File file) throws UnsupportedEncodingException, IOException {
		BufferedReader ler = new BufferedReader(new InputStreamReader(readInputStream(file), "UTF-16"));
		
        String linha;
        List<String> linhas = new ArrayList<String>();
        int i = 0;
        while ((linha = ler.readLine()) != null) {
        	if (i > 0) {
        		linha = ler.readLine();
        		linhas.add(linha);
        	}
        	i++;
        }
        ler.close();
        return linhas;
	}
	
	private boolean isCsv() throws Exception {
		if (!file.getName().toLowerCase().contains(".csv")) {
			throw new Exception("Arquivo inváldo");
		}
		return Boolean.TRUE;
	}
}
