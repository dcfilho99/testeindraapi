package com.br.teste.indra.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;

public abstract class LeitorArquivo {

	public static InputStream readInputStream(File file) throws UnsupportedEncodingException, IOException {
		return IOUtils.toBufferedInputStream(new FileInputStream(file));
	}
	
	public static byte[] readBytes(File file) throws UnsupportedEncodingException, IOException {
		byte[] retorno = IOUtils.toByteArray(readInputStream(file));
		return retorno;
	}
	
	public static File criarTempFile(byte[] origin, String preffix, String suffix) {
		try {
			File file = File.createTempFile(preffix, suffix);
			
			OutputStream os = new FileOutputStream(file);
			os.write(origin);
			os.close();
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
