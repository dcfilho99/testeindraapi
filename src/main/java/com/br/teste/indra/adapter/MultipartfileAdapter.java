package com.br.teste.indra.adapter;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.br.teste.indra.util.LeitorArquivo;

public class MultipartfileAdapter {

	private MultipartFile multipartiFile;
	
	public MultipartfileAdapter(MultipartFile file) {
		this.multipartiFile = file;
	}
	
	public File adaptar() {
		try {
			byte[] bytes = multipartiFile.getBytes();
			File file = LeitorArquivo.criarTempFile(bytes, "csv", ".csv");
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
