package com.br.teste.indra.error;

public class ResourceNotFoundDetails extends ErroDetails {
	
	private ResourceNotFoundDetails() {
		
	}

	public static final class Builder {
		private String title;
		private int status;
		private String detail;
		private long timestamp;
		private String developerMessage;
		
		private Builder() {
			
		}
		
		public static Builder newBuilder() {
			return new Builder();
		}
		
		public Builder title(String title) {
			this.title = title;
			return this;
		}
		
		public Builder status(int status) {
			this.status = status;
			return this;
		}
		
		public Builder detail(String detail) {
			this.detail = detail;
			return this;
		}
		
		public Builder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}
		
		public Builder developerMessage(String devMessage) {
			this.developerMessage = devMessage;
			return this;
		}
		
		public ResourceNotFoundDetails build() {
			ResourceNotFoundDetails d = new ResourceNotFoundDetails();
			d.setTitle(title);
			d.setStatus(status);
			d.setDetail(detail);
			d.setTimestamp(timestamp);
			d.setDeveloperMessage(developerMessage);
			return d;
		}
	}
}
