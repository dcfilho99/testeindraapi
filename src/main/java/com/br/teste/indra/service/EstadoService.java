package com.br.teste.indra.service;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.teste.indra.entity.Estado;
import com.br.teste.indra.repository.EstadoRepository;

@Service
public class EstadoService extends EntityService<Estado> {

	@Autowired
	private EstadoRepository repository;
	
	@PostConstruct
	public void init() {
		setRepository(repository);
	}
	
	public List<Estado> findByRegiaoSigla(String regiao) {
		return repository.findByRegiaoSigla(regiao);
	}
	
	public Optional<Estado> findBySigla(String sigla) {
		return repository.findBySigla(sigla);
	}
}
